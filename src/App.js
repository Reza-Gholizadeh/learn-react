import React from "react";
import TemperatureInput from "./TemperatureInput";
import * as myfunc from  './TryConvert'
import './App.css'


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      scale: "c",
      temp: ""
    };
  }
  

  
HandelChengeCelsius = (temp) => {
  this.setState({
    scale: "c",
    temp: temp
  })
}

HandelChengeFahrenheit = (temp) => {
  this.setState({
    scale: "f",
    temp: temp
  })
}

  render() {
    const celsius = this.state.scale === 'f' ? myfunc.tryConvert(this.state.temp,myfunc.toCelsius) : this.state.temp
    const fahrenheit = this.state.scale === 'c' ? myfunc.tryConvert(this.state.temp,myfunc.toFahrenheit) : this.state.temp
    return (
        <React.Fragment>
          <TemperatureInput temperature={celsius} type="c" handelChange={this.HandelChengeCelsius}/>
          <br />
          <TemperatureInput temperature={fahrenheit} type="f" handelChange={this.HandelChengeFahrenheit}/>
        </React.Fragment>
    );
  }
}

export default App;

import React from "react";

const scales = {
  c: "celsius",
  f: "fahrenheit",
};


class TemperatureInput extends React.Component {
  constructor() {
    super();
  }
  

  handelChange = (event) => {
    this.props.handelChange(event.target.value);
  };

  render() {
    return (
      <div>
        <label>{scales[this.props.type]}</label>
        <input
          type="text"
          value={this.props.temperature}
          onChange={this.handelChange}
        />
      </div>
    );
  }
}

export default TemperatureInput;
